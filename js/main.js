'use strict';

let navBarCollapsed = true;
const elNavBarList = document.querySelector('.nav-bar-list');
const toggleNavBar = () => {
    navBarCollapsed ? elNavBarList.classList.remove('collapsed') : elNavBarList.classList.add('collapsed');
    navBarCollapsed = !navBarCollapsed;
};

const initPortfolioCarousel = () => {
    const owl = $('.owl-carousel');
    owl.owlCarousel({
        // margin: 10,
        loop: true,
        nav: true,
        dots: true,
        dotsEach: true,
        responsive: {
            0: {
                items: 1
            },
        },
        navText: ['Next', 'Prev']
    });
    calculateSliderButtons();
};

const debounce = (func) => {
    let timer;
    return function (event) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(func, 100, event);
    };
};

const initListeners = () => {
    window.addEventListener('resize', debounce(() => {
        calculateSliderButtons();
    }));
};

let dots, nav;
const calculateSliderButtons = () => {
    if (!dots || !nav) {
        dots = $('.owl-dots');
        nav = $('.owl-nav');
    }
    let dotsWidth = dots.width();
    dots.css('left', (window.innerWidth - dotsWidth) / 2);
    let navWidth = nav.width();
    nav.css('left', (window.innerWidth - navWidth) / 2);
};

const init = () => {
    initListeners();
    initPortfolioCarousel();
};

$(document).ready(init);
